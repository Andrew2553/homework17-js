let student = {
    name: '',
    lastName: '',
    subjects: {},
  };
  
  // Запитати у користувача ім'я та прізвище студента
  student.name = prompt("Введіть ім'я студента:");
  student.lastName = prompt("Введіть прізвище студента:");
  
  let subject, grade;
  let badGradesCount = 0;
  let totalGrade = 0;
  let subjectsCount = 0;
  
  // Цикл для запитування назви предмету та оцінки
  while (true) {
    subject = prompt("Введіть назву предмету:");
  
    // Перевірка, чи користувач натиснув "Cancel" для назви предмету
    if (subject === null) {
      break;
    }
  
    grade = parseFloat(prompt(`Введіть оцінку для предмету "${subject}":`));
  
    // Перевірка, чи користувач натиснув "Cancel" для оцінки
    if (grade === null) {
      break;
    }
  
    student.subjects[subject] = grade;
    totalGrade += grade;
    subjectsCount++;
  
    if (grade < 4) {
      badGradesCount++;
    }
  }
  
  // Порахувати середній бал з предметів
  let averageGrade = totalGrade / subjectsCount;
  
  // Перевірка кількості поганих оцінок
  if (badGradesCount > 0) {
    console.log(`Кількість поганих оцінок: ${badGradesCount}`);
  } else {
    console.log('Студента переведено на наступний курс');
  }
  
  // Перевірка середнього балу та призначення стипендії
  if (averageGrade > 7) {
    console.log('Студенту призначено стипендію');
  }
  